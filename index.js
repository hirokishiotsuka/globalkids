var _ua = (function(u){
  return {
    Tablet:(u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1)
      || u.indexOf("ipad") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
      || u.indexOf("kindle") != -1
      || u.indexOf("silk") != -1
      || u.indexOf("playbook") != -1,
    Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
      || u.indexOf("iphone") != -1
      || u.indexOf("ipod") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
      || u.indexOf("blackberry") != -1
  }
})(window.navigator.userAgent.toLowerCase());

function device() {
	var s = screen.width;
	var i = '';
	switch(s) {
		case 414:
			i = 'i6p';
			break;
		case 375:
			i = 'i6';
			break;
		case 320:
			i = 'i5';
			break;
		default:
			i = "pc";
			break;
	}
	return i;
}
var dv = device();

(function($) {
  $j = jQuery.noConflict();
  $j(document).ready(function() {
 
    $j(function(){
      $j("#wpcf7-f14-p7-o1").validate({
    	  rules: {
    		  'text-492': "required"
    	  },
        errorPlacement: function(error,element) {
          return true;
        }
      });
      $j('#entryform').on('submit',function(){
        var check_count = $j('.check :checked').length;
        if (check_count == 0 ){
          $j('.check').css({'color':'#BC1D35'});
          return false;
        }
      });
      if(_ua.Mobile) {
        var offset = 49;
      }else{
        var offset = 158;
      }
      $j('body').sectionScroll({
        topOffset: 5
      });
    
      var voice01 = $j('#voice-01 .voice-body-block:first-child p');
      var voice01_str  = $j('#voice-01 .voice-body-block:first-child p').text();
      var voice01_slice = voice01_str.slice(0,102)+'…';
      voice01.text(voice01_slice);
    
      var voice02 = $j('#voice-02 .voice-body-block:first-child p');
      var voice02_str  = $j('#voice-02 .voice-body-block:first-child p').text();
      var voice02_slice = voice02_str.slice(0,102)+'…';
      voice02.text(voice02_slice);
    
      $j('.more').on('click',function(){
        var target = $j(this).data('voice');
        if($j(this).hasClass('close')){
          $j(this).removeClass('close');
          $j(this).addClass('open');
    		  if(_ua.Mobile) {
    			  o = '155px';
    			  w = '100%';
    			}else{
    				o = '108px';
    				w = '855px';
    			}
          $j('#'+target+' .voice-body').animate({'height': o},500,function(){
            $j('#'+target+' .voice-body p').css({'width':w});
            switch(target) {
              case 'voice-01':
                voice01.text(voice01_slice);
                break;
              case 'voice-02':
                voice02.text(voice02_slice);
                break;
            }
          });
        }else{
          $j(this).removeClass('open');
          $j(this).addClass('close');
          
    		  if(_ua.Mobile) {
    	      switch(target) {
    	        case 'voice-01':
     	          var h = '1000px';
    	        	switch(dv) {
    		        	case 'i6s':
    			        	h = '1000px';
    		        		break;
    		        	case 'i6':
    		        		h = '1050px';
    		        		break;
    		        	case 'i5':
    			        	h = '1209px';
    		        		break;
    	        	}
    	          voice01.text(voice01_str);
    	          break;
    	        case 'voice-02':
    	          var h = '1543px';
    	        	switch(dv) {
    		        	case 'i6s':
    			        	h = '1543px';
    		        		break;
    		        	case 'i6':
    		        		h = '1640px';
    		        		break;
    		        	case 'i5':
    			        	h = '1878px';
    		        		break;
    	        	}
    	          voice02.text(voice02_str);
    	          break;
    	      }
    				$j('#'+target+' .voice-body p').css({'width':'100%'});
    		  }else{
    	      switch(target) {
    	        case 'voice-01':
    	          h = '658px';
                      if (window.matchMedia('(max-width: 750px)').matches) {
                        h = '1258px';
                      }
    	          voice01.text(voice01_str);
    	          break;
    	        case 'voice-02':
    	          h = '900px';
                      if (window.matchMedia('(max-width: 750px)').matches) {
                        h = '1950px';
                      }
    	          voice02.text(voice02_str);
    	          break;
    	      }
    				$j('#'+target+' .voice-body p').css({'width':'952px'});
          }
          $j('#'+target+' .voice-body').animate({'height': h});
          
        }
      });
      $j('.entry-btn').on('click',function(){
        var speed = 400;
        var target = $j("#section04");
        var position = target.offset().top;
        $j('body,html').animate({scrollTop:position}, speed, 'swing');
        return false;
      });
    	$j('.wpcf7-select option').each(function() {
    	    var txt = jQuery(this).html();
    	    jQuery(this).html(
    	      txt.replace(/---/g,"選択")
    	    );
    	 });
    });
})(jQuery);
 
