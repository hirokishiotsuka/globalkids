<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package globalkids
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>
	<section id="primary" class="content-area home">
		<main id="main" class="site-main" role="main">


			<header class="page-header">
				<h2 class="page-title">
					<?php printf( esc_html__( '「%s」で検索した結果', '' ), '<span>' . get_search_query() . '</span>' ); ?>
				</h2>
			</header><!-- .page-header -->
			<section class="top-page post-list">
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );
				?>

			<?php endwhile; ?>
		</section>
			<?php the_posts_pagination(); ?>
		</main><!-- #main -->
	</section><!-- #primary -->
		<?php else : ?>
			<section id="primary" class="content-area">
				<main id="main" class="site-main-single" role="main">
			<?php get_template_part( 'template-parts/content', 'none' ); ?>
		</main>
	</section>
			<?php get_sidebar(); ?>
		<?php endif; ?>

<?php get_footer(); ?>
