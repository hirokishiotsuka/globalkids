<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package globalkids
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="" />
  <meta name="keywords" content="" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner">
			<div class="site-branding">
				<?php if ( get_header_image() ) : ?>
					<div class="logo">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
							<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
						</a>
					</div>
				<?php else: ?>
					<p class="site-description"><?php bloginfo( 'description' ); ?></p>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php endif; ?>
			</div><!-- .site-branding -->
			<div class="header-menu">
				<?php
				if(has_nav_menu( 'primary')) {
					echo '<nav id="site-navigation" class="main-navigation" role="navigation">';
					echo '<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">メニュー</button>';
					wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'menu_class' => 'nav-menu menu' ) );
					echo '</nav>';
				}
				?>
			</div>
		</header><!-- .site-header -->

		<?php if(is_home() && !is_paged()) {
				echo wptuts_slider_template("[slide]");
			 }
			 else {
			the_breadcrumb();
		}
			 ?>
		<div id="content" class="site-content">
