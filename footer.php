<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package globalkids
 */
?>
		</div><!-- .site-content -->
			<footer id="colophon" class="site-footer" role="contentinfo">
				<div id="pagetop" class="pagetop">
					<a href="#"><i class="fa fa-angle-double-up" aria-hidden="true"></i></a>
				</div><!-- #pagetop -->

				<div class="site-info">
					<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('copyright')): ?>
					<?php endif; ?>
				</div><!-- .site-info -->
			</footer><!-- .site-footer -->
		</div><!-- .site -->
		<?php wp_footer(); ?>
	</body>
</html>
