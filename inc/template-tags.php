<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package globalkids
 */

if ( ! function_exists( 'globalkids_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function globalkids_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() )
	);

	$posted_on = sprintf(
		esc_html_x( '%s', 'post date', 'globalkids' ), $time_string
	);

	echo '<span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'globalkids_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function globalkids_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ' ', 'globalkids' ) );
		if ( $categories_list && globalkids_categorized_blog() ) {
			$fontawesome_post = new WP_Query(
				array(
					'posts_per_page' => 1,
					'post_status' => 'publish',
					'orderby' => 'date',
					'order' => 'DESC',
					'meta_query' => array(
						array(
							'key' => 'fa-folder-open',
							'value' => '',
							'compare' => '!='
						)
					)
				)
			);
			$fontawesome_custom = array();
			if ($fontawesome_post->have_posts()) :
				while ( $fontawesome_post->have_posts() ) :
					$fontawesome_post->the_post();
					$fontawesome_custom = get_post_custom();
				endwhile;
			endif;
			wp_reset_postdata();
			if (array_key_exists("fa-folder-open", $fontawesome_custom)) {
				$fa_folder = $fontawesome_custom["fa-folder-open"][0];
				if (preg_match("/^fa-/", $fa_folder)) {
					$folder = '<i class="fa ' . $fa_folder . '" aria-hidden="true"></i>';
				} else {
					$folder = '<i class="fa">' . $fa_folder . '</i>';
				}
			} else {
				$folder = '<i class="fa fa-folder-open" aria-hidden="true"></i>';
			}
			printf( '<span class="cat-links">' . $folder . esc_html__( '%1$s', 'globalkids' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ' ', 'globalkids' ) );
		if ( $tags_list ) {
			$fontawesome_post = new WP_Query(
				array(
					'posts_per_page' => 1,
					'post_status' => 'publish',
					'orderby' => 'date',
					'order' => 'DESC',
					'meta_query' => array(
						array(
							'key' => 'fa-tags',
							'value' => '',
							'compare' => '!='
						)
					)
				)
			);
			$fontawesome_custom = array();
			if ($fontawesome_post->have_posts()) :
				while ( $fontawesome_post->have_posts() ) :
					$fontawesome_post->the_post();
					$fontawesome_custom = get_post_custom();
				endwhile;
			endif;
			wp_reset_postdata();
			if (array_key_exists("fa-tags", $fontawesome_custom)) {
				$fa_tags = $fontawesome_custom["fa-tags"][0];
				if (preg_match("/^fa-/", $fa_tags)) {
					$tags = '<i class="fa ' . $fa_tags . '" aria-hidden="true"></i>';
				} else {
					$tags = '<i class="fa">' . $fa_tags . '</i>';
				}
			} else {
				$tags = '<i class="fa fa-tags" aria-hidden="true"></i>';
			}
			printf( '<span class="tags-links">' . $tags . esc_html__( '%1$s', 'globalkids' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function globalkids_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'globalkids_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'globalkids_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so globalkids_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so globalkids_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in globalkids_categorized_blog.
 */
function globalkids_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'globalkids_categories' );
}
add_action( 'edit_category', 'globalkids_category_transient_flusher' );
add_action( 'save_post',     'globalkids_category_transient_flusher' );
