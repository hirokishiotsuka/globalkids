<?php

// Enqueue Flexslider Files
function wptuts_slider_scripts() {
    wp_enqueue_script( 'jquery' );
    wp_enqueue_style( 'camera-style', get_template_directory_uri() . '/inc/slider/css/camera.css' );
    wp_enqueue_script( 'mobile-script', get_template_directory_uri() .  '/inc/slider/js/jquery.mobile.customized.min.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'easing-script', get_template_directory_uri() .  '/inc/slider/js/jquery.easing.1.3.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'flex-script', get_template_directory_uri() .  '/inc/slider/js/camera.min.js', array( 'jquery' ), false, true );
}
add_action( 'wp_enqueue_scripts', 'wptuts_slider_scripts' );

// Save data from meta box
add_action('save_post', 'wptuts_slidelink_2_save');
function wptuts_slidelink_2_save($post_id) {
    global $post;
    global $slidelink_2_metabox;

    // verify nonce
    if(isset($_POST['wptuts_slidelink_2_meta_box_nonce'])) {
        if (!wp_verify_nonce($_POST['wptuts_slidelink_2_meta_box_nonce'], basename(__FILE__))) {
            return $post_id;
        }
    }

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    // check permissions
    if (isset($_POST['post_type'])) {
        if ('page' == $_POST['post_type']) {
            if (!current_user_can('edit_page', $post_id)) {
                return $post_id;
            }
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }
    }

    if (isset($slidelink_2_metabox['fields'])) {
        foreach ($slidelink_2_metabox['fields'] as $field) {

            $old = get_post_meta($post_id, $field['id'], true);
            $new = isset($_POST[$field['id']])? $_POST[$field['id']] : '';

            if ($new && $new != $old) {
                if($field['type'] == 'date') {
                    $new = wptuts_format_date($new);
                    update_post_meta($post_id, $field['id'], $new);
                } else {
                    if(is_string($new)) {
                        $new = $new;
                    }
                    update_post_meta($post_id, $field['id'], $new);
                }
            } elseif ('' == $new && $old) {
                delete_post_meta($post_id, $field['id'], $old);
            }
        }
    }
}

function get_the_post_thumbnail_src($img)
{
  return (preg_match('~\bsrc="([^"]++)"~', $img, $matches)) ? $matches[1] : '';
}



// Create Slider
function wptuts_slider_template() {
  $slidepost = new WP_Query(
    array(
      'numberposts'           => 8,
      'post_status'           => 'publish',
      'post_type'             => 'post',
      'meta_key'              => 'popular_post_views_count',
      'orderby'               => 'meta_value_num',
      'order'                 => 'DESC',
      'ignore_sticky_posts'   => true
    )
  );
  $counter = 0;
  if ($slidepost->have_posts()) { ?>
      <div class="camera_wrap camera_turquoise_skin">
          <?php while ( $slidepost->have_posts() ) : $slidepost->the_post();
            if ($counter > 8) { break; }
            $img_src = get_the_post_thumbnail_src(get_the_post_thumbnail());
          ?>
            <div data-thumb="<?php echo $img_src; ?>" data-src="<?php echo $img_src; ?>" data-target="<?php echo get_the_permalink(); ?>"></div>
            <?php $counter++; ?>
          <?php endwhile; ?>
      </div><!-- .flexslider -->
  <?php }
  wp_reset_postdata();
}
