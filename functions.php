<?php
/**
 *  functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package globalkids
 */

if ( ! function_exists( '_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function _setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on , use a find and replace
	 * to change '' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( '', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'メインメニュー', '' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link'
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( '_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => ''
	) ) );

	add_filter( "comments_open", "__return_false");
}
endif; // _setup
add_action( 'after_setup_theme', '_setup' );

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function _content_width() {
	$GLOBALS['content_width'] = apply_filters( '_content_width', 640 );
}
add_action( 'after_setup_theme', '_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function _widgets_init() {
	register_sidebar( array(
		'name'          => 'サイドバー',
		'id'            => 'sidebar',
		'description'   => '任意のウィジェットを登録してください。',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>'
	) );

	register_sidebar( array(
		'name'          => 'コピーライト',
		'id'            => 'copyright',
		'description'   => 'ここに「テキスト」ウィジェットを登録することで、コピーライトが表示できます。',
		'before_widget' =>'',
		'after_widget'  =>''
	) );
}
add_action( 'widgets_init', '_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function _scripts() {
	wp_enqueue_style( 'globalkids-style', get_stylesheet_uri() );

	// wp_enqueue_style( 'globalkids-font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css' );

	wp_enqueue_script( 'globalkids-script', get_template_directory_uri() . '/assets/script/bundle.min.js', array('jquery'), '', true);

	wp_enqueue_script( 'globalkids-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'globalkids-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', '_scripts' );

/**
 * Load Slider.
 */
require( get_template_directory() . '/inc/slider/slider.php' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
*/
require get_template_directory() . '/inc/jetpack.php';

/**
* Load  Breadcrumb file.
*/
require get_template_directory() . '/inc/custom/breadcrumb.php';

/**
* Load Search Filter.
*/
require get_template_directory() . '/inc/custom/searchFilter.php';
