<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package globalkids
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <div class="entry-img">
    <a href="<?php the_permalink(); ?>">
      <?php if (has_post_thumbnail()):
        the_post_thumbnail();
      else: ?>
      <img src="<?php echo get_option('ink_image') ? get_option('ink_image') : bloginfo('template_directory') . '/src/images/thumbnail.png'; ?>" />
      <?php endif; ?>
    </a>
  </div>
  <div class="info">
  <header class="entry-header">
    <?php
      the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
    ?>
  </header><!-- .entry-header -->
    <div class="entry-summary">
      <?php the_excerpt(); ?>
    </div><!-- .entry-summary -->
      <div class="entry-meta">
        <?php globalkids_entry_footer(); ?>
      </div><!-- entry-meta -->
      </div>
      <a href="<?php the_permalink(); ?>" class="read-more">もっと見る</a>
</article><!-- #post-## -->
