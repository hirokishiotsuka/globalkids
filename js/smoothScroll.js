/**
 * smoothScroll.js
 *
 */

(function($) {
  $j = jQuery.noConflict();
  $j(document).ready(function() {
    /* user clicks button on custom field, runs below code that opens new window */
    $j('a[href^=#]').click(function() {
      // スクロールの速度
      var speed = 400; // ミリ秒
      // アンカーの値取得
      var href= $j(this).attr("href");
      // 移動先を取得
      var target = $j(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      $j('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
     });
  });
})(jQuery);
