/**
 * showTooltip.js
 *
 */

(function($) {
  $j = jQuery.noConflict();
  $j(document).ready(function() {

     $j('#entry_submit_sp').on('click', function(e) {
	var notValidArr = [];
	var showErrorStr = "<ul class='tooltip'>";
	function makeErrorMessage() {
		$j('.wpcf7-not-valid').each(function() {
			notValidArr.push($j(this).attr('id'))
		});
		var dupBirthFlg = false;
		var dupPhoneFlg = false;
		for (var i = 0; i < notValidArr.length; i++) {
			console.log(notValidArr[i]);
			switch(notValidArr[i]) {
				case 'entry_name_sp':
					showErrorStr += "<li>名前を入力してください。</li>";
					break;
				case 'entry_hurigana_sp':
					showErrorStr += "<li>ふりがなを入力してください。</li>";
					break;
				case 'entry_birth_year_sp':
				case 'entry_birth_month_sp':
				case 'entry_birth_day_sp':
					if (!dupBirthFlg) {
						showErrorStr += "<li>誕生日を選択してください。</li>";
					}
					dupBirthFlg = true;
					break;
				case 'entry_region_sp':
					showErrorStr += "<li>地域を選択してください。</li>";
					break;
				case 'entry_phone_01_sp':
				case 'entry_phone_02_sp':
				case 'entry_phone_03_sp':
					if (!dupPhoneFlg) {
						showErrorStr += "<li>電話番号を入力してください。</li>";
					}
					dupPhoneFlg = true;
					break;
				case 'entry_email_sp':
					showErrorStr += "<li>メールアドレスを入力してください。</li>";
					break;
				case 'form-validation-field-0':
					showErrorStr += "<li>保有資格をチェックしてください。</li>";
					break;
				default:
					break;
			}
		}
		showErrorStr += "</ul>";
		$j('.tooltip').remove();
		if ( showErrorStr.indexOf("li") > 0 ) {
			$j('#showTooltip').append(showErrorStr);
		}
	}
	setTimeout(function(){
		makeErrorMessage();
	}, 1200);
    });


  });
})(jQuery);

