var slide = [];

function windowResize() {
  var margin = (jQuery(window).width() - 330) / 2;
  jQuery('#carouesel_list').css('left', margin + 'px');
}

function clickLabel(num) {
  if (slide[num] !== null) {
    slide[num].checked = true;
  }
  if (slide[num+5] !== null) {
    slide[num+5].checked = true;
  }
}

setInterval(function () {
  var client = [];

  for (var i = 0; i < 10; i++) {
    if (Array.isArray(slide) && slide[i] !== null && slide[i].checked) {
      client.push(slide[i]);
    }
  }

  for (var i = 0; i < client.length; i++) {
    var num = Number(client[i].id.charAt(9));

    if (num === 5) {
      if (slide[0] !== null) {
        slide[0].checked = true;
      }
      if (slide[5] !== null) {
        slide[5].checked = true;
      }
    } else {
      if (slide[num] !== null) {
        slide[num].checked = true;
      }
      if (slide[num+5] !== null) {
        slide[num+5].checked = true;
      }
    }
  }
}, 5000);

function nextForm(num) {
  var list = jQuery('#step' + num);
  var ul = jQuery('#form_step_list');
  var form = jQuery('form.wpcf7-form');

  for (var i = 0; i < ul.children().length; i++) {
    if (ul.children(i).hasClass('on')) {
      ul.children(i).removeClass('on');
    }
  }
  list.addClass('on');
  jQuery('.form_step_sp').removeClass('invisible');
  jQuery('#form_step' + num + '_sp').siblings().addClass('invisible');
  jQuery('#form_step_' + num + '_sp').siblings().addClass('invisible');
  form.animate({'right': (100 * (num - 1)) + '%'}, 500);

}

function init() {

  jQuery(window).resize(windowResize());

  for (var i = 1; i < 6; i++) {
    slide[i-1] = document.getElementById('carousel_' + i +'_ctl_pc');
  }

  for (var i = 1; i < 6; i++) {
    slide[i+4] = document.getElementById('carousel_' + i +'_ctl_sp');
  }
}

init();

jQuery("[id*=next]").on('click', function() {
  var $id = $(this).attr("id");
  var nextNum = $id.substr(-1);
  nextForm(nextNum);
});
