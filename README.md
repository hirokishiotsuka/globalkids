Global Kids
===

This is landing page for Global Kids by Hoiku Hiroba.

0. Clone this repo into 'wordpress/wp-content/themes/'
1. Run 'npm i' at document root
2. Run Apache/Nginx with port 8888
3. Run 'gulp' command at document root
4. Browser automatically open at 'http://localhost:3000/'
5. You can edit src files below './src/', Browser-sync automatically refresh browser