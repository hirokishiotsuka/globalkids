<?php
/**
* The sidebar containing the main widget area.
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package globalkids
*/

?>
<aside id="secondary" class="widget-area" role="complementary">
  <?php dynamic_sidebar( 'sidebar' ); ?>
</aside><!-- .widget-area -->
